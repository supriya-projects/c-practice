#!/bin/bash

gcc_args="-Wall -Wextra -Werror"

gcc $gcc_args -c -o add.o add.c && \
gcc $gcc_args -c -o main.o main.c && \
gcc -o test.bin add.o main.o && \
rm *.o

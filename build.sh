#!/bin/bash

file_name=$1
total_args=$#
binary_name=test.bin
script_name=$0
gcc_args="-Wall -Wextra -Werror"

function_usage() {
	echo "usage: $script_name <c_file>"
	echo
}

function_arg_validity() {
       if ! [ $total_args -eq 1 ]; then
       		echo "invalid number of arguments"
 		function_usage
		exit 1	
	fi

	if ! [ -e $file_name ]; then
		echo " $file_name does not exist"
		exit 1
		
	fi	
}	

function_arg_validity

gcc $gcc_args $file_name -o $binary_name && echo "generated binary: $binary_name"

if [ $? -eq 0 ]; then

        echo "Successfully built!"
fi


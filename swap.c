#include<stdio.h>

int main(void)
{
	int n1=12;
	int n2=6;
	printf("before swapping numbers:%d %d\n",n1,n2);

	n1=n1+n2;
	n2=n1-n2;
	n1=n1-n2;
	printf("swapped numbers are : %d %d\n",n1,n2);
	
	return 0;
}

//n1,n2=4,2;
//n1,n2=43,35;
//n1,n2=12,6;

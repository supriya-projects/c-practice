#ifndef MULTIPLY_H
#define MULTIPLY_H

#include<stdint.h>

uint16_t multiply(uint16_t num1,uint16_t num2);

#endif // MULTIPLY_H

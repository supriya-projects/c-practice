#!/bin/bash

gcc_args="-Wall -Wextra -Werror"

gcc $gcc_args -c -o multiply.o multiply.c && \
gcc $gcc_args -c -o main.o main.c && \
gcc -o test.bin multiply.o main.o && \
rm *.o


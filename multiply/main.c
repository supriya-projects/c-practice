#include<stdio.h>
#include<stdint.h>
#include"multiply.h"

int main(void)
{
	uint16_t num1 = 10;
	uint16_t num2 = 15;
	uint16_t result = multiply(num1,num2);

	printf("%d * %d =%d\n", num1,num2,result);

	return 0;

}

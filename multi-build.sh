#!/bin/bash 

dir_name=$1
total_args=$#
script_name=$0
binary_name=test.bin
gcc_args="-Wall -Wextra -Werror"

func_usage() {
        echo
        echo "Use: $script_name <dir_name>"
        echo

}

func_check_arg_validity() {
        if ! [ $total_args -eq 1 ] ; then
                echo "Invalid number of arguments"
                func_usage
                exit 1
        fi

        if ! [ -d $dir_name ] ; then
                echo "directory $dir_name does not exists."
                exit 1
        fi
}
func_check_arg_validity

file_name="$(find  $1 -name '*.c' -printf "%f\n" )"

list_of_objectfiles=""
for c_file in $file_name
do
        gcc $gcc_args -c -o $c_file.o $dir_name/$c_file
        result=$?
	if ! [ $result -eq 0 ];then
		exit 1
	fi
        list_of_objectfiles+=" $c_file.o "
done

gcc -o $binary_name $list_of_objectfiles && rm $list_of_objectfiles

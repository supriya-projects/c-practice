#include<stdio.h>
#include<stdint.h>
#include "ascii.h"

uint8_t ascii_to_lower(uint8_t ch)
{
	if(ch>='A' && ch<='Z'){
		ch = ch + 32;
	}

	return ch;
}

uint8_t ascii_to_upper(uint8_t ch)
{
	if(ch>='a' && ch<='z'){
		ch = ch - 32;
	}

	return ch;
}

void ascii_debug_info(uint8_t ch, info_function func)
{
	func(ch);
}

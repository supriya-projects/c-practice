#ifndef _ASCII_H_
#define _ASCII_H_

#include<stdint.h>

typedef void (*info_function)(uint8_t);

uint8_t ascii_to_lower(uint8_t ch);
uint8_t ascii_to_upper(uint8_t ch);
void ascii_debug_info(uint8_t ch, info_function func);

#endif // _ASCII_H

#include<stdio.h>
#include<stdint.h>
#include "ascii.h"

static void hex_value(uint8_t ch);
static void to_words(uint8_t ch);

int main(void)
{
	ascii_debug_info('A', hex_value);
	ascii_debug_info('0', to_words);

	return 0;
}

static void hex_value(uint8_t ch)
{
	printf("hex_value (%c): 0x%02X\n",ch,ch);
}

static void to_words(uint8_t ch)
{
	if(ch == '0')
		printf("%c : ZERO\n",ch);
	else if(ch == '1')
		printf("%c : ONE\n" ,ch);
	else if(ch == '2')
                printf("%c : TWO\n" ,ch);
	else if(ch == '3')
                printf("%c : THREE\n" ,ch);
	else if(ch == '4')
                printf("%c : FOUR\n" ,ch);
	else if(ch == '5')
                printf("%c : FIVE\n" ,ch);
	else if(ch == '6')
                printf("%c : SIX\n" ,ch);
	else if(ch == '7')
                printf("%c : SEVEN\n" ,ch);
	else if(ch == '8')
                printf("%c : EIGHT\n" ,ch);
	else if(ch == '9')
                printf("%c : NINE\n" ,ch);
}

#include<stdio.h>
#include<stdint.h>

uint8_t add_numbers(uint8_t *num1, uint8_t *num2, uint8_t *num3);

int main(void)
{
	uint8_t a=10;
	uint8_t b=20;
	uint8_t c=30;
	
	printf("values: %d %d %d\n",a,b,c);

	uint8_t sum=add_numbers(&a,&b,&c);
	printf("sum: %d\n",sum);

	return 0;
}

uint8_t add_numbers(uint8_t *num1, uint8_t *num2, uint8_t *num3)
{
	if(num1 == NULL || num2 == NULL || num3 == NULL)
		return 0;

	uint8_t sum;

	sum=*num1 + *num2 + *num3;

	return sum;
}

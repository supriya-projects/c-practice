#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

#define SIZE 16

uint64_t string_hex_to_int(char *hex );
int main()
{

        char hexdec[SIZE]={"0xffxx"};
        uint64_t val;

        printf("hexadecimal as a string:%s\n ",hexdec);

        val = string_hex_to_int(hexdec);

        printf("Equivalent integer value: %ld\n",val);

        return 0;
}

uint64_t string_hex_to_int(char *hex)
{
        if(hex == NULL)
                return 0;

        int i,j;
	uint64_t k;
	uint64_t base=1,sum=0,len=0;
        for(i=0;hex[i]!='\0';i++){
		len++;
	}
	for(j=len-1;j>=0;j--)
	{
         	if (hex[j]>= '0' && hex[j] <= '9')
	 	{
             		sum+=(hex[j] - '0')*base;
			base=base*16;
         	}
                else if (hex[j]>= 'A' && hex[j] <= 'F')
                {
			sum+=(hex[j] - '7')*base;
			base=base*16;
		}
		else if (hex[j]>= 'a' && hex[j] <= 'f')
                {
                        sum+=(hex[j] - 'W')*base;
                        base=base*16;
		}
	}
	for( k = 2; k <= len - 1; k++ )
	{
                if (hex[k] == 'x' || hex[k] == 'X' )
                {
                        printf("INVALID\n");
                        exit(0);
                }
        }
             
	return sum;
}

//testcases:AB,FFFF,61DA,0xff,99,7fffffff,abcd,0xffxxx1;

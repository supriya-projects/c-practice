#include<stdio.h>
#include<stdint.h>
#include "sensor_shield.h"

#define TOTAL_SAMPLES 3

int main(void)
{
	sensor_data_t s_data;
	sensor_data_t sample[TOTAL_SAMPLES];

	s_data.temp = 25;
	s_data.moisture = 30;
	
	printf("%d\n",temperature_check(s_data));
	printf("%d\n",check_if_soil_is_dry(s_data));

	sample[0].temp = 20;
	sample[1].temp = 30;
	sample[2].temp = 10;
	
	sample[0].moisture = 20;
	sample[1].moisture = 30;
	sample[2].moisture = 50;
	
	printf("temp average:%d\n",get_temperature_average(sample,TOTAL_SAMPLES));

	printf("average:%d\n",get_average(sample,TOTAL_SAMPLES));

	return 0;

}

#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include "sensor_shield.h"


uint8_t temperature_check(sensor_data_t sample)
{
		
	if(sample.temp >= 15 && sample.temp <= 35){
		printf("temperature of given sensor sample is within safe range\n");
		return true;
	}
	else
		return false;
}


uint8_t check_if_soil_is_dry(sensor_data_t sample)
{	
	if(sample.moisture < 100){
		printf("The soil is dry\n");
		return true;
	}
	else
		return false;
}

uint8_t get_temperature_average(sensor_data_t *sample, uint8_t total_samples)
{	

	if( sample == NULL )
		return 0;

	uint16_t result=0;
        uint8_t i;

	for(i=0; i<total_samples; i++){
		result += sample[i].temp;
	}

	return (result / total_samples);

}

uint8_t get_average(sensor_data_t *sample,uint8_t total_samples) 
{
	if( sample == NULL )
		return 0;
	
	uint16_t total=0;
	uint8_t i;
	uint16_t temp_avg=0 , moisture_avg=0;
	uint16_t total_temp=0 , total_moisture=0;

	for(i=0; i<total_samples; i++){
		
		total_temp += sample[i].temp;
		total_moisture += sample[i].moisture;
	}
	
	temp_avg = total_temp / total_samples;
	moisture_avg = total_moisture / total_samples;
        
	total = temp_avg + moisture_avg;
        return (uint8_t)(total / 2);

}



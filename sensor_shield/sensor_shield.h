#ifndef _SENSOR_SHIELD_H
#define _SENSOR_SHIELD_H

typedef struct __sensor_data_ {
	uint8_t temp;
	uint8_t moisture;
} sensor_data_t;

uint8_t temperature_check(sensor_data_t sample);
uint8_t check_if_soil_is_dry(sensor_data_t sample);
uint8_t get_temperature_average(sensor_data_t *sample, uint8_t total_samples);
uint8_t get_average(sensor_data_t *sample, uint8_t total_samples); 

#endif

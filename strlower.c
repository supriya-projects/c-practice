#include <stdio.h>
#define STRING_SIZE 100

void lower_string(char *str);

int main(void)
{
   	char string[STRING_SIZE]={"HELLO"};

   	lower_string(string);
   
  	printf("The string in lowercase: %s\n", string);
     
   	return 0;
}

void lower_string(char *str) 
{
	if(str == NULL)
		return;

   	int i = 0;

   	for(i=0;str[i]!='\0';i++)
   	{
	   	if (str[i] >= 'A'&& str[i] <= 'Z')
	   	{
		  	str[i]+= 32;
	   	}
   	}
  
}

//testcases: HELLO, WORLD, SUPRIYA;

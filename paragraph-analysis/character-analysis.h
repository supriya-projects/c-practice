#ifndef CHARACTER_ANALYSIS_H
#define CHARACTER_ANALYSIS_H

#include<stdint.h>

uint16_t unique_char(char *str);
uint16_t num_of_vowels(char *str);
uint16_t num_of_letters(char *str);
uint16_t num_of_whitespaces(char *str);



#endif //CHARACTER-ANALYSIS_H

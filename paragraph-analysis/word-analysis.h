#ifndef _WORD_ANALYSIS_H_
#define _WORD_ANALYSIS_H_

#include<stdint.h>

uint16_t word_count(char *str);
uint16_t num_of_sentences(char *str);
uint16_t num_of_punctuations(char *str);

#endif //_WORD_ANALYSIS_H_

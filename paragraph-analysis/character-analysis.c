#include<stdio.h>
#include<stdbool.h>
#include<stdint.h>
#include"character-analysis.h"


uint16_t unique_char(char *str)
{
	int index,j,count=0;
	bool flag= false;

	for(index=0;str[index]!='\0';index++)
	{
		//calculate the number of unique characters
		flag=false;
		for(j=index+1;str[j]!='\0';j++)
		{
			if(str[index] == str[j]) {
				flag = true;
			}
		}
		if(flag!=true) {
			count++;
		}
	}
	return count;
}

uint16_t num_of_vowels(char *str)
{
	int index,count1=0;
	for(index=0;str[index]!='\0';index++)
	{
		//calculate the number of vowels
		if(str[index] == 'a'||str[index] == 'e'||str[index] == 'i'||str[index] == 'o'||str[index] == 'u'||
			str[index] == 'A'||str[index] == 'E'||str[index] == 'I' || str[index] == 'O' || str[index] == 'U') {
	 	count1++;
		}
	}
	return count1;
}

uint16_t num_of_letters(char *str)
{
	int index,count2=0;
	for(index=0;str[index]!='\0';index++)
	{
		//calculate the number of letters
		if(str[index] != ' ' && str[index] != '?' && str[index] != '.' && str[index] != ',' && str[index] != ';' && str[index] != '!')                {
			 count2++;
		}
	}
	return count2;
}

uint16_t num_of_whitespaces(char *str)
{
	int index,count3=0;
	for(index=0;str[index]!='\0';index++)
	{
		//calculate the number of white spaces
		if(str[index] == ' ') {
			 count3++;
		}
	}

	return count3;
}

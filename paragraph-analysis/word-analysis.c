#include<stdio.h>
#include<stdint.h>
#include"word-analysis.h"

uint16_t word_count(char *str)
{
	int index=0,count=0;

	for(index=0;str[index]!='\0';index++)
	{
		//calculating number of words
		if(str[index] == ' ' && str[index+1] != ' ') {
			count++;
		}
	}
	return count;
}

uint16_t num_of_sentences(char *str)
{
	int index=0,count1=0;

        for(index=0;str[index]!='\0';index++)
        {
		//calculating number of sentences
		if(str[index] == '?' || str[index] == '.' || str[index] == '!') {
			count1++;
		}
	}
	return count1;
}
uint16_t num_of_punctuations(char *str)
{
	int index=0,count2=0;

        for(index=0;str[index]!='\0';index++)
        {
		//calculating number of punctuations
		if(str[index] == '?' || str[index] == '.' || str[index] == ',' || str[index] == ';' || str[index] == '!') {
			count2++;
		}

	}
	return count2;
}

//testcases:hello world.
//          good morning,everyone.

#include<stdio.h>
#include<stdint.h>
#include"character-analysis.h"
#include"word-analysis.h"

int main(void)
{
        char str[50]={"Good morning,everyone."};
        uint16_t count=0,count1=0,count2=0,count3=0,count4=0,count5=0,count6=0;

        count=unique_char(str);
        printf("number of unique characters in string:%d\n",count);

        count1=num_of_vowels(str);
        printf("number of vowels in string:%d\n",count1);

        count2=num_of_letters(str);
        printf("number of letters in string:%d\n",count2);

        count3=num_of_whitespaces(str);
        printf("number of whitespaces in string:%d\n",count3);

	count4=word_count(str);
        printf("The number of words in string:%d\n",count4+1);

        count5=num_of_sentences(str);
        printf("number of sentences in string:%d\n",count5);

        count6=num_of_punctuations(str);
        printf("number of punctuation marks in string:%d\n",count6);

        return 0;
}


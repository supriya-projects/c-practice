#include<stdio.h>
#include<string.h>

int main(void)
{
	int len,n=3;
	char str1[10],str2[10],str3[10],str4[10],str5[10],str6[10],res,result;


	printf("enter string1:\n");
	scanf("%s",str1);
	printf("enter string2:\n");
	scanf("%s",str2);
	printf("enter string3:\n");
	scanf("%s",str3);
	printf("enter string4:\n");
        scanf("%s",str4);
	printf("enter string5:\n");
	scanf("%s",str5);
	printf("enter string6:\n");
	scanf("%s",str6);


	strcpy(str2,str1);
	printf("copying string1 to string2 :%s\n",str2);
	

	strcat(str1,str3);
	printf("concatenation of string1 and string3 : %s\n",str1);


	len=strlen(str3);
	printf("length of the string3 : %d\n",len);


	res=strcmp(str3,str4);
	if(res==0)
	{
		printf("comparing str3 and str4 : strings are equal\n");
	}
	else
		printf("comparing str3 and str4 : strings are not equal\n");
	

	strncat(str3,str4,n);
	printf("concatenation of string3 and %d characters of string4 : %s\n",n,str3);


	strncpy(str4,str5,n);
	printf("copying %d characters of str5 to str4:%s\n",n,str4);


	result = strncmp(str6,str5,n);
        if(result<0)
                printf("comparing %d characters:string6 is less than string5\n",n);
        else if(result>0)
                printf("comparing %d characters:string6 is greater than string5\n",n);
        else
                printf("comparing %d characters:string6 is equal to string5\n",n);


	return 0;
}

//string1=hello, string2=world, string3=supriya, string4=supriya, string5=ubuntu, string6=bye;
//string1=firmware, string2=bootcamp, string3=hello, string4=embedded, string5=systems, string6=system;

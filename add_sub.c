#include<stdio.h>
#include<stdint.h>

uint16_t  number_function(uint16_t *num1, uint16_t num2,uint16_t *subtracted_value);

int main(void)
{
        uint16_t a=50;
        uint16_t b=20;
        uint16_t subtracted_value=0;
        uint16_t sum=0;

        printf("values: %d %d\n",a,b);

        sum=number_function(&a,b,&subtracted_value);

        printf("sum: %u\n",sum);
        printf("subtracted_value: %u\n",subtracted_value);

        return 0;
}

uint16_t number_function(uint16_t *num1, uint16_t num2,uint16_t *subtracted_value)

{
        if(num1 == NULL || subtracted_value == NULL )
        {
                return 0;
        }

        uint16_t sum;

        sum = *num1 + num2;
        *subtracted_value = *num1 - num2;

	return sum;

}

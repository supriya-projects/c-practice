#include<stdint.h>
#include<stdio.h>

int main() 
{
	uint8_t count = 10;
	uint8_t *count_address = NULL;

	count_address = &count;

	printf("address of count : %p (%p)\n" ,count_address,&count);
	printf("value of count:%d\n",*count_address);
	printf("sizeof count: %lu\n",sizeof(count));
	printf("sizeof count_address:%lu\n",sizeof(count_address));
	
	char ch = 'E';
	char *ch_address = NULL;
	
	ch_address = &ch;

	printf("address of ch : %p (%p)\n" ,ch_address,&ch);
        printf("value of ch:%c\n",*ch_address);
        printf("sizeof ch: %lu\n",sizeof(ch));
        printf("sizeof ch_address:%lu\n",sizeof(ch_address));
	
	*ch_address += 16;
	printf("value of ch:%c\n",*ch_address);


	int num1 = 8;
        int *num1_address = NULL;

        num1_address = &num1;

        printf("address of num1 : %p (%p)\n" ,num1_address,&num1);
        printf("value of count:%d\n",*num1_address);
        printf("sizeof num1: %lu\n",sizeof(num1));
        printf("sizeof num1_address:%lu\n",sizeof(num1_address));
	
	*num1_address += 8;
	printf("value of num1:%d\n",*num1_address);

	uint16_t num2 = 15;
        uint16_t *num2_address = NULL;

        num2_address = &num2;

        printf("address of num2 : %p (%p)\n" ,num2_address,&num2);
        printf("value of num2:%d\n",*num2_address);
        printf("sizeof num2: %lu\n",sizeof(num2));
        printf("sizeof num2_address:%lu\n",sizeof(num2_address));
	

	return 0;
}

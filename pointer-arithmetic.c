#include<stdio.h>
#include<stdint.h>

void pointer_math8_example(uint8_t *source_array);
void pointer_math16_example(uint16_t *source_array);
void print_list(uint8_t *list_of_numbers,uint16_t list_size);
void pointer_math_example(int *source_array);


int main(void)
{
	uint8_t bit8_array[10] = {0,1,2,3,4,5,6,7,8,9};
	uint16_t bit16_array[10]={0,1,2,3,4,5,6,7,8,9};
	int bit_array[10]={0,1,2,3,4,5,6,7,8,9};

	print_list(bit8_array,10);
	pointer_math8_example(bit8_array);
	pointer_math16_example(bit16_array);
	pointer_math_example(bit_array);
	
	return 0;

}

void pointer_math8_example(uint8_t *source_array)
{
	if (source_array == NULL)
		return;

	printf("addresss of array: %p\n",source_array);
        printf("value of first element: %d\n",source_array[0]);
	source_array++;
	printf("addresss of array after incrementing: %p\n",source_array);
	printf("value of first element: %d\n",source_array[0]);

}

void pointer_math16_example(uint16_t *source_array)
{
        if (source_array == NULL)
                return;

        printf("addresss of array: %p\n",source_array);
        printf("value of first element: %d\n",source_array[0]);
        source_array++;
        printf("addresss of array after incrementing: %p\n",source_array);
        printf("value of first element: %d\n",source_array[0]);

}

void pointer_math_example(int *source_array)
{
        if (source_array == NULL)
                return;

        printf("addresss of array: %p\n",source_array);
        printf("value of first element: %d\n",source_array[0]);
        source_array++;
        printf("addresss of array after incrementing: %p\n",source_array);
        printf("value of first element: %d\n",source_array[0]);

}


void print_list(uint8_t *list_of_numbers,uint16_t list_size)
{
        printf(" list of numbers: ");
        for (uint16_t index=0; index < list_size; index++) {
                printf("%d",list_of_numbers[index]);
        }
        printf(" \n");
}


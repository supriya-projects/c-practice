#include<stdio.h>
#include<stdbool.h>
#include<stdint.h>

bool valid_bin_character(char ch);
uint8_t bin_number_from_character(char ch);
bool binary_string_to_int(char *binary_string, uint16_t *num);

int main(void)
{
        char binary_string[]={"1001"};
        bool result;
        uint16_t number;

        result = binary_string_to_int(binary_string,&number);
        if(result == false)
                return -1;

        printf("integer value is %d\n",number);
        return 0;
}

bool binary_string_to_int(char *binary_string, uint16_t *num)
{
        if(binary_string == NULL || num == NULL)
                return 0;

        uint16_t string_len,index,integer=0;

        //calculating string length
        for(index=0;binary_string[index]!='\0';index++){

        }
        string_len=index;

        if(string_len == 0)
        {
                printf("empty string\n");
                return false;
        }
        //check for prefix 0b
        if(string_len >= 2)
        {
                if(binary_string[0]=='0' && binary_string[1] == 'b')
                        index = 2;
                else
                        index = 0;
        }
	//converting string to number
        for(; binary_string[index]!='\0';index++)
        {
                if(valid_bin_character(binary_string[index]) == false){
                        printf("error: \"%c\" (position %d) is invalid binary character in %s\n",binary_string[index],index,binary_string);
                        return false;
                }

                integer = integer << 1;
                integer = (integer | (bin_number_from_character(binary_string[index])));
        }

        *num = integer;
        return true;
}

bool valid_bin_character(char ch)
{
        if( ch >= '0' && ch <= '1')
                return true;
        else
	        return false;
}

uint8_t bin_number_from_character(char ch)
{
        if( ch >= '0' && ch <= '1' )
                return (ch - '0');
        else
                return 0;
}

//testcases:0b1111,0b11h,10001101

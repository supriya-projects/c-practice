# c-practice

c-practice repository for practicing C programming

# List of files

1. add.c  
    -code to perform sum of elements of array
1. add_3num.c  
    -Function to add three numbers using pointers
1. add_sub.c  
    -program to add and subtract two numbers using pointers
1. alpha.c  
    -function to check alphanumeric characters
1. arr_n_cpy.c  
    -function to copy n bytes from pointer source to another
1. arrcpy.c  
    -function to copy one array to another
1. ascii.c  
    -function to print ascii table
1. bin_to_int.c  
    -program to convert binary string to integer
1. binary_print.c  
    -program to convert decimal to binary
1. case.c  
    -program to convert the case of character
1. concatenation.c  
    -program to concatenate two strings
1. enum_debug.c  
    -to print enum for weekdays
1. fib.c  
    -program to print fibonacci series
1. hex_to_int.c  
    -program to convert hexadecimal string to integer
1. lsb.c  
    -source code to check LSB is 1
1. mem_func.c  
    -program to demonstrate memory library functions
1. multiply.c  
    -function to multiply a number without using multiplication operator
1. num.c  
    -function to accept a number and returns corresponding number
1. operator.c  
    -switch case example
1. pointer-arg.c
1. pointer-array.c
1. pointer-demo.c  
    -pointer basic demo program
1. pointer-arithmetic.c  
    -pointer arithmetic example program
1. rtruncate.c  
    -program to reverse truncating
1. strcpy.c  
    -program to copy one string to another
1. string-library.c  
    -program to demonstrate string library functions
1. strlen.c  
    -program to calculate string length using pointers
1. swap.c  
    -program to swap two numbers
1. strlower.c  
    -program for case conversion
1. alpha.c
1. cntrl.c
1. xdigit.c
1. space.c
1. punct.c  
    -ASCII ctype functions
1. truncate.c  
    -program to truncate string array
1. unique.c  
    -program to count unique characters
1. addnumbers  
    -program to add numbers using 2 software modules
1. multiply  
    -program to multiply using modules
1. paragraph-analysis  
    -program to count words,letters,sentences,vowels,whitespaces,unique characters using modules

# How to run each program

```sh
one time usage: 
$ sudo apt-get install build-essential

To build and run:
$ ./build.sh <c_file_name>

$ ./<binary_name>.bin

```

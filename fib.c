#include<stdio.h>
int fibonacci(int);

int main(void)
{
    int terms;

    printf("Enter terms:\n ");
    scanf("%d",&terms);

    for(int n = 0; terms > n; n++)
    {
        printf("%d\n ",fibonacci(n));
    }

    return 0;

}

int fibonacci(int num)
{


    if(num < 2)
    {
        return num;
    }

    else
    {

        return fibonacci(num-1) + fibonacci(num-2);
    }

}

//input
//terms= 10,6,15,25;

#include<stdio.h>
#include<stdint.h>

uint8_t add_numbers(uint8_t *num1,uint8_t num2);

int main(void)
{
	uint8_t a=15;
	uint8_t b=5;
	printf("values: %d %d\n",a,b);
	uint8_t sum=add_numbers(&a,b);
	printf("sum=%d\n",sum);
	printf("values after calling function: %d %d\n",a,b);
	return 0;
}

uint8_t add_numbers(uint8_t *num1,uint8_t num2)
{
	if (num1 == NULL)
		return 0;
	uint8_t sum;

	sum=*num1 + num2;
	
	*num1+=5;
	num2+=3;

	return sum;
}

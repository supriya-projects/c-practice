#include<stdio.h>
#include<stdint.h>

#define PI 3.14

uint8_t count = 0;
uint8_t *count_addr;


typedef uint16_t (*math_func)(uint8_t ,uint8_t);

uint16_t add_numbers(uint8_t first_num,uint8_t second_num);
uint16_t subtract_numbers(uint8_t first_num,uint8_t second_num);

uint16_t do_arithmetic(math_func arithmetic_func,uint8_t num1,uint8_t num2);

int main(void)
{
	uint8_t a =2;
	uint8_t b =1;
	uint16_t result;
	math_func my_math_operation;

	my_math_operation = add_numbers;
	result = do_arithmetic(my_math_operation, a, b);
	printf("result (%d, %d) = %d\n",a ,b, result);
	
	my_math_operation = subtract_numbers;
	result = do_arithmetic(my_math_operation, a, b);
        printf("result (%d, %d) = %d\n",a ,b, result);

	return 0;
}

uint16_t do_arithmetic(math_func arithmetic_func,uint8_t num1,uint8_t num2)
{
	uint16_t result;
	result = arithmetic_func(num1,num2);
	return result;
}

uint16_t add_numbers(uint8_t first_num,uint8_t second_num)
{
	return (uint16_t)(first_num + second_num);
}
uint16_t subtract_numbers(uint8_t first_num,uint8_t second_num)
{
        return (uint16_t)(first_num - second_num);
}

#include<stdio.h>
#define STRING_SIZE 4

int string_int(char *string );
int main()
{

        char str[STRING_SIZE]={"-555"};
        int val;

        printf("integer as a string:%s\n ",str);

        val = string_int(str);

        printf("Equivalent integer value: %d",val);

        return 0;
}

int string_int(char *string)
{
	if(string == NULL)
                return 0;

        int sum=0,i=0,check_negative=1;
	if (string[0] == '-'){
		check_negative=-1;
		i++;
	}

        for(;string[i]!='\0';i++){

                if(string[i] > '9')
                {
                        printf("invalid\n");
                        return 0;
                }
                else
                {
                        sum = sum*10 + (string[i] - '0');
                }
	}
	
	return check_negative*sum;
}

//testcases: "1234","2387","1001"
//for negative:-122,-223,-555;

#include<stdio.h>
#include<stdint.h>
#include<stddef.h>

void copy(void *dest,void *src,size_t n_bytes);
void print_list(uint8_t *list_of_numbers,uint16_t list_size);

int main(void)
{
        uint8_t arr[ ]={1,2,3,4,5,6,7,8,9};

        uint8_t n = 5;

        copy(arr ,arr+2, n);

        print_list(arr, (sizeof(arr)/sizeof(arr[0])));

        return 0;
}

void copy(void *dest,void *src,size_t n_bytes)
{
        if( src == NULL || dest == NULL )
                return;

        uint16_t index;

        char *cdest = (char*)dest;
        char *csrc = (char*)src;

        if(src < dest)
        {
                for(index=n_bytes; index > 0; index--)
                {
                        cdest[index] = csrc[index];
                }
        }
        else if(src > dest)
        {

                for(index=0; index <= n_bytes; index++)
                {
                        cdest[index] = csrc[index];
                }
        }

}
void print_list(uint8_t *list_of_numbers,uint16_t list_size)
{
        printf(" list of numbers: ");
        for (uint16_t index=0; index < list_size; index++) {
                printf("%d ",list_of_numbers[index]);
        }
        printf(" \n");
}
                   


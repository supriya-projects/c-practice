#include<stdio.h>
#include<stdint.h>

void print_list(uint8_t *list_of_numbers,uint16_t list_size);

int main(void)
{
	uint8_t numbers[10]={5, 10, 15, 20, 25, 30, 35, 40, 45, 50};

	printf("(1) list of numbers: ");
	for (uint8_t index=0; index < sizeof(numbers); index++) {
		printf("%d",numbers[index]);
	}
	printf(" \n");
	print_list(numbers, sizeof(numbers));

	return 0;
}

void print_list(uint8_t *list_of_numbers,uint16_t list_size)
{
	printf("(2) list of numbers: ");
	for (uint16_t index=0; index < list_size; index++) {
                printf("%d",list_of_numbers[index]);
        }
	printf(" \n");

	printf("(3) list of numbers: ");
	for (uint16_t index=0; index < list_size; index++) {
                printf("%d", *(list_of_numbers+index));
        }
	printf(" \n");

}


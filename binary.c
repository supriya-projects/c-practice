#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define SIZE 16

int string_bin_to_int(char *string );
int main(void)
{

        char str[SIZE]={"0b00bb111"};
        int val;

        printf("binary as a string:%s\n ",str);

        val = string_bin_to_int(str);

        printf("Equivalent integer value: %d",val);

        return 0;
}

int string_bin_to_int(char *string)
{
        if(string == NULL)
                return 0;

	int len,sum=0,base=1;
	len=strlen(string);

	for (int i = len - 1; i >= 0; i--)
       	{
        	if (string[i] >= '0' && string[i] <='1')
		{
            		sum += (string[i] - '0')*base;
        		base = base * 2;
		}
	}
                
	for(int j = 2; (j <= len - 1); j++)
	{
		if ( string[j] == 'b' )
		{
			printf("INVALID\n");
			exit(0);
		}
	}

	return sum;
}

//testcases: 0b11111111,11,1111,1001,10010001,0b11bbbb1

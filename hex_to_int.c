#include<stdio.h>
#include<stdbool.h>
#include<stdint.h>

bool valid_hex_character(char ch);
uint8_t hex_number_from_character(char ch);
bool hexstring_to_int(char *hexstring, uint64_t *num);

int main(void)
{
	char hexstring[]={"0x61DA"};
	bool result;
	uint64_t number;

	result = hexstring_to_int(hexstring,&number);
	if(result == false)
		return -1;

	printf("integer value is %ld (0x%lX)\n",number,number);
	return 0;
}

bool hexstring_to_int(char *hexstring, uint64_t *num)
{
	if(hexstring == NULL || num == NULL)
		return 0;

	uint64_t string_len,index,integer=0;

	//calculating string length
	for(index=0;hexstring[index]!='\0';index++){

	}
	string_len=index;

	if(string_len == 0)
	{
		printf("empty string\n");
		return false;
	}
	//check for prefix 0x
	if(string_len >= 2)
	{
		if(hexstring[0]=='0' && (hexstring[1] == 'x' || hexstring[1] == 'X'))
			index = 2;
		else
			index = 0;
	}

	for(; hexstring[index]!='\0';index++)
	{
		if(valid_hex_character(hexstring[index]) == false){
			printf("error: \"%c\" (position %ld) is invalid hex character in %s\n",hexstring[index],index,hexstring);
			return false;
		}
		
		integer = integer << 4;
		integer = (integer | (hex_number_from_character(hexstring[index])));
	}

	*num = integer;
	return true;
}

bool valid_hex_character(char ch)
{
	if( ch >= '0' && ch <= '9')
		return true;
	else if( ch >= 'a' && ch <= 'f')
		return true;
	else if( ch >= 'A' && ch <= 'F')
		return true;
	else
		return false;
}

uint8_t hex_number_from_character(char ch)
{
	if( ch >= '0' && ch <= '9' )
		return (ch - '0');
	else if( ch >= 'a' && ch <= 'f')
		return (ch - 'a' + 10);
	else if( ch >= 'A' && ch <= 'F')
		return (ch - 'A' + 10);
	else
		return 0;
}

//testcases:0xff,abcd,123,oxffffff,0x61DA

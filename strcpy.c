#include<stdio.h>
#define STRING_SIZE 100

void string_copy(char *src,char *dest);

int main(void)
{
	char destination[STRING_SIZE],source[STRING_SIZE]={"helloworld"};

	printf("original string: %s\n",source);

	string_copy(source,destination);

	printf("copied string: %s\n",destination);

	return 0;
}

void string_copy(char *src,char *dest)
{
	int i=0;

	for(i=0;src[i]!='\0';i++)
	{
		dest[i] = src[i];

	}
}

//testcases: hello, helloworld

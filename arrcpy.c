#include<stdio.h>

void array_copy(int *src,int *dest,int len);

int main(void)
{
        int destination[50],source[]={1,2,3,4,5};
	int i,len;
	
	len=sizeof(source)/sizeof(source[0]);

        array_copy(source,destination,len);

        printf("copied array:\n");
	for(i=0;i<len;i++)
	{
		printf("%d\n",destination[i]);
		
	}

        return 0;
}

void array_copy(int *src,int *dest,int len)
{
	if ( src == NULL || dest == NULL )
		return;

       	int i;

       	for(i=0; i<len; i++) 
       	{
	       	dest[i] = src[i];
       	}
}

//testcases: "1 2 3 4 5", "2 3 8 7", "5 6 7 8 9 10"

#include<stdio.h>
#include<stdint.h>
#include "math_operations.h"

static uint16_t add_numbers(uint8_t first_num,uint8_t second_num)
{
        return (uint16_t)(first_num + second_num);
}
static uint16_t subtract_numbers(uint8_t first_num,uint8_t second_num)
{
        return (uint16_t)(first_num - second_num);
}
static uint16_t multiply_numbers(uint8_t first_num,uint8_t second_num)
{
        return (uint16_t)(first_num * second_num);
}

uint16_t math_operation(enum operation_type op,uint8_t num1,uint8_t num2)
{
	uint16_t result;
	switch (op){
                case MATH_ADD:
                        result = add_numbers(num1, num2);
			break;
                case MATH_SUBTRACT:
                        result = subtract_numbers(num1, num2);
                        break;
                case MATH_MULTIPLY:
                        result = multiply_numbers(num1, num2);
                        break;
                }


        return result;

}



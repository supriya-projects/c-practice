#include<stdio.h>
#include<stdint.h>
#include "math_operations.h"

int main(void)
{
        uint8_t a =2;
        uint8_t b =1;
        uint16_t result;
	enum operation_type op=MATH_ADD;
	
	result = math_operation(op,a, b);

	if(op == MATH_ADD){
        	printf("result (%d, %d) = %d\n",a ,b, result);
	}
	else if(op == MATH_SUBTRACT){
                printf("result (%d, %d) = %d\n",a ,b, result);
	}
	else if(op == MATH_MULTIPLY){
                printf("result (%d, %d) = %d\n",a ,b, result);
	}
	return 0;
}


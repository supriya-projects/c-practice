#ifndef _MATH_OPERATIONS_H
#define _MATH_OPERATIONS_H

#include<stdint.h>

enum operation_type { 
		     MATH_ADD, 
		     MATH_SUBTRACT, 
	       	     MATH_MULTIPLY 
		    };

uint16_t math_operation(enum operation_type,uint8_t ,uint8_t);

#endif //_MATH_OPERATIONS_H

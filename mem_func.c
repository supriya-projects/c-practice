#include<stdio.h>
#include<string.h>
#include<stdint.h>

void print_list(uint8_t *list_of_numbers,uint16_t list_size);

int main(void)
{
	uint8_t val[10]={1,2,3,4,5,6,7,8,9,10};

	memset(val, 2, 9);
	print_list(val,10);

	char src[10]={"hello"};
        char dest[10]={"world"};
	printf("before memcpy:%s\n",dest);
	memcpy(dest,src,5);
	printf("after memcpy:%s\n",dest);

	char src1[10]={"good"},dest1[10]={"morning"};
	printf("before memmove dest:%s\n",dest1);
	memmove(dest1,src1,4);
	printf("after memmove dest:%s\n",dest1);
	
	return 0;

}

void print_list(uint8_t *list_of_numbers,uint16_t list_size)
{
        printf(" list of numbers: ");
        for (uint16_t index=0; index < list_size; index++) {
                printf("%d",list_of_numbers[index]);
        }
        printf(" \n");
}
